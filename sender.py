# -*- coding: utf-8 -*-

import telebot
from sqlalchemy import and_
from config import token
from common import session
from db import *

bot = telebot.TeleBot(token)
users = session.query(User).all()

category_hardcoded = 'krasnoe_selo'


def send_to_all_users():
    for user in users:
        yn, last_for_subs = have_news_for_user(user.id)
        if yn > 0:
            print('ParseMun INFO: news not in DB: {} - {}'.format(yn, last_for_subs))
            all_news_by_stream = session.query(News).\
                        filter(and_(News.category == category_hardcoded, News.id >= last_for_subs)).\
                        all()
            for news in all_news_by_stream:
                send_formated_message(user.id, news.title, news.url)


def send_formated_message(user, title, url):
    print(user)
    print(title)
    print(url)
    text = '{} {}'.format(title, url)
    try:
        bot.send_message(user, text, parse_mode='HTML')
        update_subscribe(user, url)
    except Exception as e:
        print('ParseMun ERROR (sfm): {}'.format(e))


def update_subscribe(user, url):
    news_id, category = get_news_id(url)
    set_news_id(user, news_id, category)


def set_news_id(user, news_id, category):
    subs_row = session.query(Subscribe).\
                    filter(and_(Subscribe.category == category, Subscribe.user_id == user)).\
                    first()
    subs_row.last_news_id = news_id
    try:
        session.commit()
    except Exception as e:
        print('ParseMun ERROR (sni): {}'.format(e))


def get_news_id(url):
    news_from_db = session.query(News).\
                    filter(News.url == url).\
                    first()
    return news_from_db.id, news_from_db.category


def have_news_for_user(userid):
    print(userid)
    try:
        last_job_in_stream = session.query(News).\
                            filter(News.category == category_hardcoded).\
                            order_by(News.id.desc()).first().id
        print('ParseMun INFO (hnu): {}'.format(last_job_in_stream))
        last_job_for_subscribe = session.query(Subscribe).\
                            filter(and_(Subscribe.category == category_hardcoded, Subscribe.user_id == userid)).\
                            first().last_news_id
        print('ParseMun INFO (hnu2): {}'.format(last_job_for_subscribe))
    except Exception as e:
        print('ParseMun ERROR (hnu): {}'.format(e))
        # exit(1)
        return 0, None  # skip this

    if last_job_for_subscribe < last_job_in_stream:
        return last_job_in_stream - last_job_for_subscribe, last_job_for_subscribe
    else:
        return 0, None


send_to_all_users()
