from db import *
from sqlalchemy.orm import sessionmaker


Session = sessionmaker(bind=engine)
session = Session()


def news_exist(news_link):
    cur = session.execute("SELECT url FROM news")
    links = cur.fetchall()
    if any(news_link in s[0] for s in links):
        print('ParseMun INFO: link in DB: {}'.format(news_link))
        return True
    else:
        return False


def save_news_to_db(url, title, chan):
    if news_exist(url):
        print(title)
    else:
        print('ParseMun INFO: Have news\n{}'.format(title))
        news_row = News(
            title=title,
            date_parse='1',  # TODO сохранять нормальную дату
            url=url,
            category=chan
        )
        session.add(news_row)
        session.commit()

