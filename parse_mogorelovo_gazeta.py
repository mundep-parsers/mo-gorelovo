# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
from common import save_news_to_db

url_to_parse = 'http://mogorelovo.ru/gazeta/'
domain = 'http://mogorelovo.ru'
category = 'krasnoe_selo'


def parse_site(url, chan):
    page = requests.get(url).content
    soup = BeautifulSoup(page, features='html.parser')
    all_gazettes_div = soup.findAll('div', {"id": "content-block"})
    all_gazettes = all_gazettes_div[0].findAll('p')
    for gazette in all_gazettes:
        save_news_to_db(domain + gazette.a.get('href'), gazette.a.text, chan)


if __name__ == '__main__':
    print("ParseMO_gorelovo_gazeta Started")
    try:
        parse_site(url_to_parse, category)
    except Exception as e:
        print('ParseGorelGaz ERROR: {}'.format(e))
