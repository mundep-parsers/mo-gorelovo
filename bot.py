# -*- coding: utf-8 -*-

# from db import *
from config import token
import telebot

bot = telebot.TeleBot(token)


@bot.message_handler(commands=['start', 'help'])
def handle_start(msg):
    full = str(msg.chat.id)  # for debug
    bot.send_message(msg.chat.id, 'Твой ID: {} :: {}'.format(msg.from_user.id, full))


if __name__ == '__main__':
    print("ParseMun Started")
    bot.polling(none_stop=True)
