# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
from common import save_news_to_db
from db import *
from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=engine)
session = Session()

url_to_parse = 'http://mogorelovo.ru/novosti/'
domain = 'http://mogorelovo.ru'
channel = 'krasnoe_selo'

# TODO парсить и другие разделы, такие как http://mogorelovo.ru/gazeta/


def parse_site(url, chan):
    page = requests.get(url).content
    soup = BeautifulSoup(page, features="html.parser")
    all_news = soup.findAll('div', class_='news')
    for news in all_news:
        save_news_to_db(domain + news.a['href'], news.a.text, chan)


if __name__ == '__main__':
    print("ParseMun Started")
    try:
        parse_site(url_to_parse, channel)
    except Exception as e:
        print('ParseMun ERROR: {}'.format(e))
