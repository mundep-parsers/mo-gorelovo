from db import *
from common import session

metadata = Base.metadata
metadata.create_all(engine)


def initial_data():
    try:
        user_row = User(
            id=-226471258,
            role=0
        )
        session.add(user_row)
        session.commit()
    except Exception as e:
        print('ParseMun ERROR: can\'t init -- {}'.format(e))

    try:
        subscribtion_row = Subscribe(
            category='krasnoe_selo',
            user_id=-226471258,
            last_news_id=0
        )
        session.add(subscribtion_row)
        session.commit()
    except Exception as e:
        print('ParseMun ERROR: can\'t init -- {}'.format(e))


initial_data()
