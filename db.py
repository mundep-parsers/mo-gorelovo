import os
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String

Base = declarative_base()
basedir = os.path.abspath(os.path.dirname(__file__))
db_path = 'sqlite:///' + os.path.join(basedir, './.mundep.db')
engine = create_engine(db_path, echo=False)


class News(Base):
    __tablename__ = 'news'
    id = Column(Integer, primary_key=True)
    title = Column(String)
    date_parse = Column(String)
    url = Column(String, unique=True)
    category = Column(String)


class Subscribe(Base):
    __tablename__ = 'subscribes'
    id = Column(Integer, primary_key=True)
    category = Column(String)
    user_id = Column(Integer)
    last_news_id = Column(Integer)


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    role = Column(Integer)
